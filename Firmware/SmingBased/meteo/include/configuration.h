#ifndef INCLUDE_CONFIGURATION_H_
#define INCLUDE_CONFIGURATION_H_
#include <SmingCore.h>


/**
 *
 */
typedef struct
{
	float	t_lamp;
	float   t_air;
	float	i_lamp;
	int		dimmer;
} PhytoParameters;

extern HardwarePWM HW_pwm;

// If you want, you can define WiFi settings globally in Eclipse Environment Variables
#ifndef WIFI_SSID
        #define WIFI_SSID "PleaseEnterSSID" // Put you SSID and Password here
        #define WIFI_PWD "PleaseEnterPass"
#endif

#include <SmingCore.h>

const char THERM_CONFIG_FILE[] = ".therm.conf"; // leading point for security reasons :)

struct NetworkConfig
{
	NetworkConfig()
	{
		StaEnable = 1; //Enable WIFI Client
	}

	String StaSSID;
	String StaPassword;
	uint8_t StaEnable;

// ThermControl settings


};

NetworkConfig loadConfig();
void saveConfig(NetworkConfig& cfg);

extern NetworkConfig ActiveConfig;

#endif /* INCLUDE_CONFIGURATION_H_ */
