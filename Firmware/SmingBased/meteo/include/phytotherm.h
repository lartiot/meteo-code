#ifndef INCLUDE_PHYTOTHERM_H_
#define INCLUDE_PHYTOTHERM_H_
#include <configuration.h>
#include <SmingCore.h>
#include "meteo.h"

extern unsigned long counter; // Kind of heartbeat counter
extern PhytoParameters phytoParameters;
extern MeteoData 	meteoData;
extern MeteoConfig 	meteoConfig;


const uint8_t ConfigJsonBufferSize = 200; // Application configuration JsonBuffer size ,increase it if you have large config
const uint16_t ConfigFileBufferSize = 2048; // Application configuration FileBuffer size ,increase it if you have large config

//Webserver
void startWebServer();

#endif /* INCLUDE_HEATCONTROL_H_ */
