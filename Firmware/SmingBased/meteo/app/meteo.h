/*
 * meteo.h
 *
 *  Created on: 3 ���. 2016 �.
 *      Author: spark
 */

#ifndef APP_METEO_H_
#define APP_METEO_H_

enum
{
	expanderAdr = 0x38,

};

/** -------------------------------------------
 * ������ ESP-12x
 */
enum
{
	PIN_OW1		= 0, //!< PIN_OW1
	PIN_TXD		= 1, //!< PIN_TXD
	PIN_INT		= 2, //!< PIN_INT
	PIN_RXD		= 3, //!< PIN_RXD
	PIN_SDA 	= 4, //!< PIN_SDA
	PIN_SCL 	= 5, //!< PIN_SCL
	PIN_IO1 	= 12,//!< PIN_IO1
	PIN_IO2 	= 13,//!< PIN_IO2
	PIN_IO3 	= 14,//!< PIN_IO3
	PIN_MQEN	= 15,//!< PIN_MQEN
	PIN_IO0		= 16 //!< PIN_IO0
};

/** -------------------------------------------
 * ������ IO-����������
 */
enum
{
	EXP_IO0 	= 1 << 0,  //!< EXP_IO0
	EXP_IO1 	= 1 << 1,  //!< EXP_IO1
	EXP_IO2 	= 1 << 2,  //!< EXP_IO2
	EXP_IO3 	= 1 << 3,  //!< EXP_IO3
	EXP_IO4 	= 1 << 4,  //!< EXP_IO4
	EXP_RO1   	= 1 << 5,  //!< EXP_RO1
	EXP_RO2   	= 1 << 6,  //!< EXP_RO2
	EXP_SELAIN	= 1 << 7   //!< EXP_SELAIN
};
/** -------------------------------------------
 * ��������� ������ ��� �������� ��������� ������������
 */
typedef struct METEODATA_S
{
	/** ����������� - ��������� */
	struct TH_SENS_S
	{
		float temperature;
		float humidity;
		float heatindex;
		String description;
	} TH_Sens;
	/** ����������� - ����������� �������� */
	struct TP_SENS_S
	{
		float temperature;
		float pressure;
	} TP_Sens;
	/** MQ-135 - ���������� CO2 */
	struct CO2SENS_S
	{
		float CO2ppm;
	} CO2Sens;
	struct OUTDOORTEMP
	{
		float temperature;
	} OutDoorTemp;
	struct FLAGS_S
	{
		bool presentBMP;
		bool presentExpIO;
		bool presentDHT;
	} flags;
} MeteoData;

/** -------------------------------------------
 * ��������� ������ ��� �������� ��������� ������������
 */
typedef struct METEOCONFIG_S
{
	/** ������� ��������� ��������� */
	union jumpers
	{
		uint8_t		raw;
		struct {
			char dio1_sda	:1;
			char dio2_scl	:1;
			char exp_int	:1;
			char ain_att	:1;
			char ain_bias	:1;
		};
	};
	/** ��� ������� DHT */
	uint8_t dht_type;
} MeteoConfig;




#endif /* APP_METEO_H_ */
