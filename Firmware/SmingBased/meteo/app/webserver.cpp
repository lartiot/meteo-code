#include <phytotherm.h>


bool serverStarted = false;
HttpServer server;

void onIndex(HttpRequest &request, HttpResponse &response)
{
	response.setCache(86400, true); // It's important to use cache for better performance.
	response.sendFile("index.html");
}

void onConfiguration(HttpRequest &request, HttpResponse &response)
{

	if (request.getRequestMethod() == RequestMethod::POST)
	{
		debugf("Update config");
		// Update config
		if (request.getBody() == NULL)
		{
			debugf("NULL bodyBuf");
			return;
		}
		else
		{
			StaticJsonBuffer<ConfigJsonBufferSize> jsonBuffer;
			JsonObject& root = jsonBuffer.parseObject(request.getBody());
			root.prettyPrintTo(Serial); //Uncomment it for debuging

			if (root["StaSSID"].success()) // Settings
			{
				uint8_t PrevStaEnable = ActiveConfig.StaEnable;

				ActiveConfig.StaSSID = String((const char *)root["StaSSID"]);
				ActiveConfig.StaPassword = String((const char *)root["StaPassword"]);
				ActiveConfig.StaEnable = root["StaEnable"];

				if (PrevStaEnable && ActiveConfig.StaEnable)
				{
					WifiStation.enable(true);
					WifiAccessPoint.enable(false);
					WifiStation.config(ActiveConfig.StaSSID, ActiveConfig.StaPassword);
				}
				else if (ActiveConfig.StaEnable)
				{
					WifiStation.enable(true, true);
					WifiAccessPoint.enable(false, true);
					WifiStation.config(ActiveConfig.StaSSID, ActiveConfig.StaPassword);
				}
				else
				{
					WifiStation.enable(false, true);
					WifiAccessPoint.enable(true, true);
					WifiAccessPoint.config("MeteoBox", "admin", AUTH_WPA2_PSK);
				}
			}
		}
		saveConfig(ActiveConfig);
	}
	else
	{
		response.setCache(86400, true); // It's important to use cache for better performance.
		response.sendFile("config.html");
	}
}

void onConfiguration_json(HttpRequest &request, HttpResponse &response)
{
	JsonObjectStream* stream = new JsonObjectStream();
	JsonObject& json = stream->getRoot();

	json["StaSSID"] = ActiveConfig.StaSSID;
	json["StaPassword"] = ActiveConfig.StaPassword;
	json["StaEnable"] = ActiveConfig.StaEnable;

	response.sendJsonObject(stream);
}
void onFile(HttpRequest &request, HttpResponse &response)
{
	String file = request.getPath();
	if (file[0] == '/')
		file = file.substring(1);

	if (file[0] == '.')
		response.forbidden();
	else
	{
		if( strstr( file.c_str( ) , ".c." ) ||  strstr( file.c_str( ) , ".gz." ) )
			response.setHeader("Content-Encoding", "gzip");
		response.setCache(86400, true); // It's important to use cache for better performance.
		response.sendFile(file);
	}
}

void onAJAXGetState(HttpRequest &request, HttpResponse &response)
{
	JsonObjectStream* stream = new JsonObjectStream();
	JsonObject& json = stream->getRoot();

	if (request.getRequestMethod() == RequestMethod::POST)
	{
		debugf( "[POST STATE] %s" , request.getBody() );
//		String val = request.getPostParameter("value" , "0");
//		phytoParameters.dimmer = val.toInt();
		//HW_pwm.analogWrite( 14, phytoParameters.dimmer );
//		Serial.println("----------------------------------");
//		Serial.println( val );

	}else
	{
		JsonArray& array = json.createNestedArray( "data" );

		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "temp-tp";
			object["value"] = meteoData.TP_Sens.temperature;
			array.set( 0 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "press";
			object["value"] = meteoData.TP_Sens.pressure;
			array.set( 1 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "temp-th";
			object["value"] = meteoData.TH_Sens.temperature;
			array.set( 2 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "hymid";
			object["value"] = meteoData.TH_Sens.humidity;
			array.set( 3 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "heatidx";

			String str( meteoData.TH_Sens.heatindex , 1 );
			str += String(" (") + meteoData.TH_Sens.description +String(")");
			object["value"] = str;
			array.set( 4 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "hwrtc";
			DateTime time( RTC.getRtcSeconds() );
			object["value"] = time.toShortTimeString( true );
			array.set( 5 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "outdoortemp";
			object["value"] = meteoData.OutDoorTemp.temperature;
			array.set( 6 , object );
		}
		{
			JsonObject& object = array.createNestedObject( );
			object["name"] 	= "spco2";
			object["value"] = meteoData.CO2Sens.CO2ppm;
			array.set( 7 , object );
		}
	}
	response.sendJsonObject(stream);
}


void startWebServer()
{
	if (serverStarted) return;

	server.listen(80);
	server.addPath("/", onIndex);
	server.addPath("/config", onConfiguration);
	server.addPath("/config.json", onConfiguration_json);
	server.addPath("/state", onAJAXGetState);
	server.setDefaultHandler(onFile);
	serverStarted = true;

	if (WifiStation.isEnabled())
		debugf("STA: %s", WifiStation.getIP().toString().c_str());
	if (WifiAccessPoint.isEnabled())
		debugf("AP: %s", WifiAccessPoint.getIP().toString().c_str());
}
