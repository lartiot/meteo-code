#include <SmingCore.h>
#include <libraries/OneWire/OneWire.h>
#include <libraries/DS18S20/ds18s20.h>
#include <libraries/BMP180/BMP180.h>
#include <server.h>
#include <phytotherm.h>
#include <pcf8574.h>
#include <Libraries/DHT/DHT.h>
#include <sming/wiring/WVector.h>
#include "sming/core/RBootClass.h"
#include "meteo.h"

/** ------------------------------------------- @{ */
rBootHttpUpdate* otaUpdater = 0;
DS18S20 ReadTemp;
Timer procTimer;
PCF8574 expander;
DHT* dht = nullptr;
BMP180 barometer;
MeteoConfig meteoConfig;
MeteoData   meteoData;
/** ------------------------------------------- @} */
#define ROM_0_URL	"pihost.local:1880/rom/"

void OtaUpdate_CallBack(bool result) {
    if (result == true) {
        // success
        uint8 slot;
        slot = rboot_get_current_rom();
        if (slot == 0) slot = 1; else slot = 0;
        // set to boot new rom and then reboot
        Serial.printf("Firmware updated, rebooting to rom %d...\r\n", slot);
        rboot_set_current_rom(slot);
        System.restart();
    } else {
        // fail
        Serial.println("Firmware update failed!");
    }
}

void OtaUpdate() {

    uint8 slot;
    rboot_config bootconf;

    // need a clean object, otherwise if run before and failed will not run again
    if (otaUpdater) delete otaUpdater;
    otaUpdater = new rBootHttpUpdate();

    // select rom slot to flash
    bootconf = rboot_get_config();
    slot = bootconf.current_rom;
    if (slot == 0) slot = 1; else slot = 0;

#ifndef RBOOT_TWO_ROMS
    // flash rom to position indicated in the rBoot config rom table
    otaUpdater->addItem(bootconf.roms[slot], ROM_0_URL);
#else
    // flash appropriate rom
    if (slot == 0) {
        otaUpdater->addItem(bootconf.roms[slot], ROM_0_URL);
    } else {
        otaUpdater->addItem(bootconf.roms[slot], ROM_1_URL);
    }
#endif

    // use user supplied values (defaults for 4mb flash in makefile)
//    if (slot == 0) {
//        otaUpdater->addItem(RBOOT_SPIFFS_0, SPIFFS_URL);
//    } else {
//        otaUpdater->addItem(RBOOT_SPIFFS_1, SPIFFS_URL);
//    }

    // set a callback
    otaUpdater->setCallback(OtaUpdate_CallBack);

    // start update
    otaUpdater->start();
}


void readOwTemp()
{
	uint8_t a = 0;
	uint64_t info;

	if (!ReadTemp.MeasureStatus())  // the last measurement completed
	{
      if (ReadTemp.GetSensorsCount())   // is minimum 1 sensor detected ?
	    for(a=0;a<ReadTemp.GetSensorsCount();a++)   // prints for all sensors
	    {
	      if ( ReadTemp.IsValidTemperature( a ) )   // temperature read correctly ?
	      {
	    	  float temp = ReadTemp.GetCelsius( a );
	    	  debugf("\tTemperature: %2.1f\xB0 C" , temp );
	    	  meteoData.OutDoorTemp.temperature = temp;
	      }
//	      else
//	    	  debugf("\tTemperature not valid");
	    }
		ReadTemp.StartMeasure();  // next measure, result after 1.2 seconds * number of sensors
	}
	else
		debugf("No valid Measure so far! wait please");


}

void rbootDelegate(int resultCode) {

	Serial.printf("rBoot resultcode =  %d\r\n", resultCode );
}


#define KEY_NAME		"name"
#define KEY_VALUE		"value"
#define KEY_TYPE		"type"


/** -------------------------------------------
 * ������ ������
 */
void readData()
{
	static bool state = false;
	state ^= true;

	if( meteoData.flags.presentExpIO )  expander.write( state ? EXP_RO1 : EXP_RO2 );
	meteoData.CO2Sens.CO2ppm = ( ( float ) system_adc_read( ) ) / 1.0230f;
	debugf( "\t MQ: %4.1fmV" , meteoData.CO2Sens.CO2ppm );

	if( meteoData.flags.presentExpIO )  expander.write( ( state ? EXP_RO1 : EXP_RO2 ) | EXP_SELAIN );
	meteoData.CO2Sens.CO2ppm = ( ( float ) system_adc_read( ) ) / 1.0230f;
	debugf( "\tADC: %4.1fmV" , meteoData.CO2Sens.CO2ppm );


	if( meteoData.flags.presentBMP )
	{
		meteoData.TP_Sens.pressure    = barometer.GetPressure();
		meteoData.TP_Sens.temperature = barometer.GetTemperature();
		debugf( "\tTemperature: %2.1f\xB0 C    Pressure: %5.1fPa\r\n" , meteoData.TP_Sens.temperature , meteoData.TP_Sens.pressure  );
	}

	if( meteoData.flags.presentDHT )
	{
		TempAndHumidity dhtValue;
		dht->readTempAndHumidity( dhtValue  );
		meteoData.TH_Sens.humidity    = dhtValue.humid;
		meteoData.TH_Sens.temperature = dhtValue.temp;
		meteoData.TH_Sens.heatindex   = dht->getHeatIndex( dhtValue.temp , dhtValue.humid );
		debugf( "\tTemperature: %2.1f\xB0 C    Humidity: %3.1f%\r\n" , dhtValue.temp , dhtValue.humid );
		debugf( "\tHeat Index: %3.1f" , dht->getHeatIndex( dhtValue.temp , dhtValue.humid ) );
		String desc = "ok!";
		if( dht->isTooCold( dhtValue.temp , dhtValue.humid  ) )   desc = String( "to cold!" );
		if( dht->isTooHot( dhtValue.temp , dhtValue.humid  ) )    desc = String( "to hot!" );
		if( dht->isTooHumid( dhtValue.temp , dhtValue.humid  ) )  desc = String( "to humid!" );
		if( dht->isTooDry( dhtValue.temp , dhtValue.humid  ) )    desc = String( "to dry!" );
		debugf( "%s\r\n" , desc.c_str() );
		meteoData.TH_Sens.description = desc;
	}

	readOwTemp( );

	int io1 = digitalRead( PIN_IO1 );
	int io2 = digitalRead( PIN_IO2 );
	debugf( "\tIO1: %1d; IO2: %1d" , io1 , io2  );

}

/** -------------------------------------------
 * ������������� DHT
 */
void dht_init( uint8_t type )
{
	if( dht != nullptr ) delete dht;
	dht = new DHT( PIN_OW1 , type );
	dht->begin();
}

/** -------------------------------------------
 * �������������
 */
void init()
{
	System.setCpuFrequency( eCF_160MHz );
//	spiffs_mount( ); // Mount file system, in order to work with files
	int slot = rboot_get_current_rom();
	if (slot == 0) {
	    debugf("trying to mount spiffs at %x, length %d", RBOOT_SPIFFS_0 + 0x40200000, SPIFF_SIZE);
	    spiffs_mount_manual(RBOOT_SPIFFS_0 + 0x40200000, SPIFF_SIZE);
	} else {
	    debugf("trying to mount spiffs at %x, length %d", RBOOT_SPIFFS_1 + 0x40200000, SPIFF_SIZE);
	    spiffs_mount_manual(RBOOT_SPIFFS_1 + 0x40200000, SPIFF_SIZE);
	}
	Serial.begin( SERIAL_BAUD_RATE ); // 115200 by default
	Serial.systemDebugOutput( true );
	Serial.commandProcessing( true );

	Serial.printf("add spiff roms\r\n");

	server_init();

	meteoData.flags.presentBMP   = false;
	meteoData.flags.presentExpIO = false;
	meteoData.flags.presentDHT   = true;

	expander.begin( expanderAdr , PIN_SCL , PIN_SDA );
	if( !expander.EnsureConnected() )
		Serial.println("Could not connect to PCF8574.");
	else
	{
		for( int i = 0 ; i < 8 ; i++ ) expander.pinMode( i, OUTPUT );
		meteoData.flags.presentExpIO   = true;

	}

	if( !barometer.EnsureConnected( ) )
		Serial.println("Could not connect to BMP180.");
	else
	{
		barometer.Initialize( );
		meteoData.flags.presentBMP   = true;
	}

	meteoConfig.dht_type = DHT22;
	dht_init( meteoConfig.dht_type );

	debugf( "Wait onewire." );
	ReadTemp.Init( PIN_IO3 );
	ReadTemp.StartMeasure( ); // first measure start,result after 1.2 seconds * number of sensors

	pinMode( PIN_MQEN , OUTPUT );
	digitalWrite( PIN_MQEN , HIGH );

	pinMode( PIN_IO1 , INPUT_PULLUP );
	pinMode( PIN_IO2 , INPUT_PULLUP );

	debugf( "Start read meteo data." );
	procTimer.initializeMs( 1200 * 2, readData ).start( );   // every 10 seconds
}
