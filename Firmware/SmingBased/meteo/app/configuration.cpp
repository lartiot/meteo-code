#include <phytotherm.h>

NetworkConfig ActiveConfig;


NetworkConfig loadConfig()
{
	StaticJsonBuffer< ConfigJsonBufferSize > jsonBuffer;
	NetworkConfig cfg;
	if (fileExist(THERM_CONFIG_FILE))
	{
		int size = fileGetSize(THERM_CONFIG_FILE);
		char* jsonString = new char[size + 1];
		fileGetContent(THERM_CONFIG_FILE, jsonString, size + 1);
		JsonObject& root = jsonBuffer.parseObject(jsonString);

		JsonObject& network = root["network"];
		cfg.StaSSID = String( ( const char* )network[ "StaSSID" ] );
		cfg.StaPassword = String( ( const char* )network[ "StaPassword" ] );
		cfg.StaEnable = network[ "StaEnable" ];
		JsonObject& sensors = root[ "sensors" ];
		meteoConfig.dht_type = sensors[ "DHTType" ];

		delete[] jsonString;
	}
	else
	{
		//Factory defaults if no config file present
		cfg.StaSSID = WIFI_SSID;
		cfg.StaPassword = WIFI_PWD;
	}
	return cfg;
}

void saveConfig(NetworkConfig& cfg)
{
	StaticJsonBuffer< ConfigJsonBufferSize > jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();

	JsonObject& network = jsonBuffer.createObject();
	root["network"] = network;
	network["StaSSID"] = cfg.StaSSID.c_str();
	network["StaPassword"] = cfg.StaPassword.c_str();
	network["StaEnable"] = cfg.StaEnable;
	JsonObject& sensors = jsonBuffer.createObject();
	root[ "sensors" ] = sensors;
	sensors[ "DHTType" ] = meteoConfig.dht_type;

	char buf[ ConfigFileBufferSize ];
	root.prettyPrintTo( buf, sizeof( buf ) );
	fileSetContent( THERM_CONFIG_FILE, buf );
}


