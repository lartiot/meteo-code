$( document ).ready(function() 
{
	var dataDescriptions = [{ idname:'temp-tp',
							  name:'Temperature (BMP)', 
							  units:'&degC'		, 
							  defvalue:'---',
							  classname : 'digitalOut' 
							} , 
	                        { 
							  idname:'temp-th' 		, 
							  name:'Temperature (DHT)'  , 
							  units:'&degC'		, 
							  defvalue:'---',
							  classname : 'digitalOut' 
							},
							{
  							  idname:'hymid'   		, 
							  name:'Relative hymidity' 	, 
							  units:'%' 		, 
							  defvalue:'---',
							  classname : 'digitalOut' 
							},
							{ 
							  idname:'press', 
							  name:'Air preassure' , 
							  units:'mmHg', 
							  defvalue:'---',
							  classname : 'digitalOut'
							},
							{ 
							  idname:'heatidx', 
							  name:'Heat index', 
							  units:'', 
							  defvalue:'---',
							  classname : 'digitalOut' 
							},
							{ 
							  idname:'spco2', 
							  name:'CO2', 
							  units:'&permil;', 
							  defvalue:'---',
							  classname : 'digitalOut' 
							},
							{ 
							  idname:'hwrtc', 
							  name:'Time', 
							  units:'', 
							  defvalue:'--:--:--',
							  classname : 'digitalOut' 
							},
							{ 
							  idname:'outdoortemp'  , 
							  name:'Outdoor temperature', 
							  units:'&degC'		, 
							  defvalue:'---',
							  classname : 'digitalOut' 
							},
							{
								idname:'dimmerval',
								name:'Dimmer power',
								units:'%', 
							    defvalue:'50',
							    classname : 'digitalIn' 
							},
							{						
								idname:'startdiag',
								name:'Start diagnostic',
								units:'', 
							    defvalue:'0',
							    classname : 'booleanOut' 
							}
					];
	
	// -------------------------------------------------
	// class digitalOut
	function digitalOut( option )
	{  
		var value = option.defvalue; 
		var obj = option;
		obj.col_xs = 12;
		obj.col_md = 6;
		function create( )
		{
			console.log( obj );
			$("#js-contentRow").append(	'<div class="col-xs-12 col-md-6"><div class="panel panel-default"><div class="panel-heading">'+	
					'<h3 class="panel-title">'+ obj.name + '</h3></div><div class="panel-body"><div class="form-row">'+
					'<h2 class="text-center main"><span id="' + obj.idname + '">' + obj.defvalue + '</span> ' +
					'<span>' +  obj.units + '</span></h2></div></div></div></div>');
			
		};
		function setValue( val )
		{ 
			value = val;
			$('#' + obj.idname ).html( value );
		};
		function getValue( ) { return value; }
		
		this.setValue = setValue; 
		this.getValue = getValue; 
		this.create = create;
	};
	
	// -------------------------------------------------
	// class digitalIn
	function digitalIn( option )
	{  
		var value = option.defvalue; 
		var obj = option;
		obj.col_xs = 12;
		obj.col_md = 6;
		function create( )
		{
			console.log( obj );
			$("#js-contentRow").append(	'<div class="col-xs-12 col-md-6"><div class="panel panel-default"><div class="panel-heading">'+	
					'<h3 class="panel-title">'+ obj.name + '</h3></div><div class="panel-body"><div class="form-row">'+
					'<h3 class="text-center "> <input id="' + obj.idname + '" type="range" value="'+ obj.defvalue  +'" name="'+ obj.units + '">' +
					'</h3></div></div></div></div>');
//            $("input[id='+ obj.name +']").TouchSpin({
//                initval: 40
//            });	
		};
		function setValue( val )
		{ 
			value = val;
			$('"#' + obj.idname + '"').html( value );
		};
		function getValue( ) { return value; }
		
		this.setValue = setValue; 
		this.getValue = getValue; 
		this.create = create;
	};
	
	// ---------------------------------------------------
	// class Alert
	function alertOut( option )//message, type, closeDelay ) 
	{
		var opt = option;

		opt.message = opt.message || ''; 
		opt.type    = opt.type    || "info";// info success, warning, danger
		opt.timer   = opt.timer   || 1000;

		function create( )
		{
			if ($("#js-messageDiv").length == 0) {
				$("body").append( $('<div id="js-messageDiv" style="position: fixed; width: 50%; left: 25%; top: 10%;">') );
			}
			
			var tmrid = (function(){ var tmrid = setTimeout( function()  
										{  console.log(tmrid);  
											$('#js-message-'+tmrid ).alert('close'); 
										} , opt.timer ); 
										return tmrid; }
									)(); 
			var alert = $('<div id=js-message-'+tmrid+' class="alert alert-' + opt.type + ' fade in">')
				.append(
					$('<button type="button" class="close" data-dismiss="alert">')
					.append("&times;")
				)
				.append(opt.message);
			$("#js-messageDiv").prepend( alert );
		}
		
		this.create = create;
		
	}
	
	// ---------------------------------------------------
	//
	var widgets = []; 
	function render()
	{
		$("#js-contentRow").empty();
		dataDescriptions.forEach( function( current ) 
		{	 
			if( current.classname == 'digitalOut' )
				widgets[ String(current.idname) ] = new digitalOut( current );
			else
				if( current.classname == 'digitalIn' )
					widgets[ String(current.idname) ] = new digitalIn( current );
				else return;

			$("#js-contentRow").append( widgets[ String(current.idname) ].create( ) );
		} );
	}
	 
	 
	// ---------------------------------------------------
	//
	$("#js-objBtn").click( function() {
		alert( "Oz!!" );
	} );
	
	$("#js-btnConfig").click( function() {
			alert( "Config!!" );
	} );
	
	$("#js-messageBtn").click( function() {
		var opt = { message : "Тут все хорошо!!!" , type : "success" , timer : 5000 };
		var alr = new alertOut( opt );
		alr.create( );
		//showAlert( "Тут все плохо!!!" , "danger" 	, 5000 );
	} );

	// -------------------------------------------------------
	render();
	
	// -------------------------------------------------------
	(function worker() {                                                                       
		$.getJSON('/state.json', function( dataValues ) {
			dataValues.data.forEach( function( current ) {
				widgets[ String( current.name ) ].setValue( current.value );});
			setTimeout( worker, 10000 ); 
	}); }) ( );
});